<?php

namespace LogisticsX\Misc;

use OpenAPI\Runtime\ResponseTypes as AbstractResponseTypes;

class ResponseTypes extends AbstractResponseTypes
{
    public array $types = [
        'getAddressCollection' => [
            '200.' => 'LogisticsX\\Misc\\Model\\Address\\Address\\Read[]',
        ],
        'postAddressCollection' => [
            '201.' => 'LogisticsX\\Misc\\Model\\Address\\Address\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'defaultAddressCollection' => [
            '200.' => 'LogisticsX\\Misc\\Model\\Address\\Address\\Read[]',
        ],
        'system_defaultAddressCollection' => [
            '200.' => 'LogisticsX\\Misc\\Model\\Address\\Address\\Read[]',
        ],
        'getAddressItem' => [
            '200.' => 'LogisticsX\\Misc\\Model\\Address\\Address\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putAddressItem' => [
            '200.' => 'LogisticsX\\Misc\\Model\\Address\\Address\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteAddressItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getAttachmentCollection' => [
            '200.' => 'LogisticsX\\Misc\\Model\\Attachment\\Attachment\\Read[]',
        ],
        'postAttachmentCollection' => [
            '201.' => 'LogisticsX\\Misc\\Model\\Attachment\\Attachment\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'downloadAttachmentItem' => [
            '200.' => 'LogisticsX\\Misc\\Model\\Attachment\\Attachment\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getByRecordAttachmentCollection' => [
            '200.' => 'LogisticsX\\Misc\\Model\\Attachment\\Attachment\\Read[]',
        ],
        'getByAttachmentTypeAttachmentCollection' => [
            '200.' => 'LogisticsX\\Misc\\Model\\Attachment\\Attachment\\Read[]',
        ],
        'getAttachmentItem' => [
            '200.' => 'LogisticsX\\Misc\\Model\\Attachment\\Attachment\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putAttachmentItem' => [
            '200.' => 'LogisticsX\\Misc\\Model\\Attachment\\Attachment\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteAttachmentItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getSystemConfigCollection' => [
            '200.' => 'LogisticsX\\Misc\\Model\\SystemConfig\\SystemConfig\\Read[]',
        ],
        'postSystemConfigCollection' => [
            '201.' => 'LogisticsX\\Misc\\Model\\SystemConfig\\SystemConfig\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getSystemConfigItem' => [
            '200.' => 'LogisticsX\\Misc\\Model\\SystemConfig\\SystemConfig\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putSystemConfigItem' => [
            '200.' => 'LogisticsX\\Misc\\Model\\SystemConfig\\SystemConfig\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteSystemConfigItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
    ];
}
