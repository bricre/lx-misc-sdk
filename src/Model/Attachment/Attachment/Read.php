<?php

namespace LogisticsX\Misc\Model\Attachment\Attachment;

use OpenAPI\Runtime\AbstractModel;

/**
 * Attachment.
 */
class Read extends AbstractModel
{
    /**
     * @var string
     */
    public $id = null;

    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $clientCode = null;

    /**
     * @var string|null
     */
    public $filename = null;

    /**
     * @var string|null
     */
    public $suffix = null;

    /**
     * @var string|null
     */
    public $path = null;

    /**
     * @var string
     */
    public $recordType = null;

    /**
     * @var string
     */
    public $recordId = null;

    /**
     * @var string|null
     */
    public $attachmentType = null;

    /**
     * @var string|null
     */
    public $additionalReference = null;

    /**
     * @var string|null
     */
    public $presignedUrl = null;

    /**
     * @var string|null
     */
    public $presignedUrlExpiryTime = null;
}
