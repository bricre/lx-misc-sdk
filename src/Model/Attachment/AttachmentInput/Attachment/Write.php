<?php

namespace LogisticsX\Misc\Model\Attachment\AttachmentInput\Attachment;

use OpenAPI\Runtime\AbstractModel;

/**
 * Attachment.
 */
class Write extends AbstractModel
{
    /**
     * @var string
     */
    public $filename = null;

    /**
     * @var string|null
     */
    public $suffix = null;

    /**
     * @var string
     */
    public $recordType = null;

    /**
     * @var string|null
     */
    public $recordId = null;

    /**
     * @var string|null
     */
    public $attachmentType = null;

    /**
     * @var string|null
     */
    public $additionalReference = null;

    /**
     * @var string
     */
    public $base64Content = null;
}
