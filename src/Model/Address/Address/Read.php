<?php

namespace LogisticsX\Misc\Model\Address\Address;

use OpenAPI\Runtime\AbstractModel;

/**
 * Address.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $clientCode = null;

    /**
     * @var bool
     */
    public $isDefault = null;

    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string
     */
    public $contact = null;

    /**
     * @var string|null
     */
    public $businessName = null;

    /**
     * @var string
     */
    public $addressLine1 = null;

    /**
     * @var string|null
     */
    public $addressLine2 = null;

    /**
     * @var string|null
     */
    public $addressLine3 = null;

    /**
     * @var string|null
     */
    public $city = null;

    /**
     * @var string|null
     */
    public $county = null;

    /**
     * @var string
     */
    public $countryIso = null;

    /**
     * @var string|null
     */
    public $postcode = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var string
     */
    public $usage = null;
}
