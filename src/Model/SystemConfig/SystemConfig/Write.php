<?php

namespace LogisticsX\Misc\Model\SystemConfig\SystemConfig;

use OpenAPI\Runtime\AbstractModel;

/**
 * SystemConfig.
 */
class Write extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string|null
     */
    public $value = null;
}
