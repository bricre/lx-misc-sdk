<?php

namespace LogisticsX\Misc\Api;

use LogisticsX\Misc\Model\Attachment\Attachment\Read;
use LogisticsX\Misc\Model\Attachment\AttachmentInput\Attachment\Write;

class Attachment extends AbstractAPI
{
    /**
     * Retrieves the collection of Attachment resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'uuid'	string
     *                       'uuid[]'	array
     *                       'filename'	string
     *                       'filename[]'	array
     *                       'suffix'	string
     *                       'suffix[]'	array
     *                       'recordType'	string
     *                       'recordType[]'	array
     *                       'recordId'	string
     *                       'recordId[]'	array
     *                       'attachmentType'	string
     *                       'attachmentType[]'	array
     *                       'additionalReference'	string
     *                       'additionalReference[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'order[id]'	string
     *                       'order[recordId]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getAttachmentCollection',
        'GET',
        'api/misc/attachments',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Attachment resource.
     *
     * @param Write $Model The new Attachment resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postAttachmentCollection',
        'POST',
        'api/misc/attachments',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a Attachment resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return Read|null
     */
    public function downloadItem(string $uuid): ?Read
    {
        return $this->request(
        'downloadAttachmentItem',
        'GET',
        "api/misc/attachments/download/$uuid",
        null,
        [],
        []
        );
    }

    /**
     * Retrieves the collection of Attachment resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'uuid'	string
     *                       'uuid[]'	array
     *                       'filename'	string
     *                       'filename[]'	array
     *                       'suffix'	string
     *                       'suffix[]'	array
     *                       'recordType'	string
     *                       'recordType[]'	array
     *                       'recordId'	string
     *                       'recordId[]'	array
     *                       'attachmentType'	string
     *                       'attachmentType[]'	array
     *                       'additionalReference'	string
     *                       'additionalReference[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'order[id]'	string
     *                       'order[recordId]'	string
     *
     * @return Read[]|null
     */
    public function getByRecordCollection(array $queries = []): ?array
    {
        return $this->request(
        'getByRecordAttachmentCollection',
        'GET',
        'api/misc/attachments/record/{recordType}/{recordId}',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves the collection of Attachment resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'uuid'	string
     *                       'uuid[]'	array
     *                       'filename'	string
     *                       'filename[]'	array
     *                       'suffix'	string
     *                       'suffix[]'	array
     *                       'recordType'	string
     *                       'recordType[]'	array
     *                       'recordId'	string
     *                       'recordId[]'	array
     *                       'attachmentType'	string
     *                       'attachmentType[]'	array
     *                       'additionalReference'	string
     *                       'additionalReference[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'order[id]'	string
     *                       'order[recordId]'	string
     *
     * @return Read[]|null
     */
    public function getByTypeCollection(array $queries = []): ?array
    {
        return $this->request(
        'getByAttachmentTypeAttachmentCollection',
        'GET',
        'api/misc/attachments/{recordType}/{recordId}/{attachmentType}',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a Attachment resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $uuid): ?Read
    {
        return $this->request(
        'getAttachmentItem',
        'GET',
        "api/misc/attachments/$uuid",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Attachment resource.
     *
     * @param string $uuid  Resource identifier
     * @param Write  $Model The updated Attachment resource
     *
     * @return Read
     */
    public function putItem(string $uuid, Write $Model): Read
    {
        return $this->request(
        'putAttachmentItem',
        'PUT',
        "api/misc/attachments/$uuid",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Attachment resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $uuid): mixed
    {
        return $this->request(
        'deleteAttachmentItem',
        'DELETE',
        "api/misc/attachments/$uuid",
        null,
        [],
        []
        );
    }
}
