<?php

namespace LogisticsX\Misc\Api;

use LogisticsX\Misc\Model\Address\Address\Read;
use LogisticsX\Misc\Model\Address\Address\Update;
use LogisticsX\Misc\Model\Address\Address\Write;

class Address extends AbstractAPI
{
    /**
     * Retrieves the collection of Address resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'clientCode'	string
     *                       'isDefault'	boolean
     *                       'isDefault[]'	array
     *                       'name'	string
     *                       'contact'	string
     *                       'contact[]'	array
     *                       'businessName'	string
     *                       'businessName[]'	array
     *                       'addressLine1'	string
     *                       'addressLine1[]'	array
     *                       'addressLine2'	string
     *                       'addressLine2[]'	array
     *                       'addressLine3'	string
     *                       'addressLine3[]'	array
     *                       'city'	string
     *                       'city[]'	array
     *                       'county'	string
     *                       'county[]'	array
     *                       'countryIso'	string
     *                       'countryIso[]'	array
     *                       'postcode'	string
     *                       'postcode[]'	array
     *                       'telephone'	string
     *                       'telephone[]'	array
     *                       'email'	string
     *                       'email[]'	array
     *                       'usage'	string
     *                       'usage[]'	array
     *                       'order[id]'	string
     *                       'order[clientId]'	string
     *                       'order[usage]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getAddressCollection',
        'GET',
        'api/misc/addresses',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Address resource.
     *
     * @param Write $Model The new Address resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postAddressCollection',
        'POST',
        'api/misc/addresses',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves the collection of Address resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'clientCode'	string
     *                       'isDefault'	boolean
     *                       'isDefault[]'	array
     *                       'name'	string
     *                       'contact'	string
     *                       'contact[]'	array
     *                       'businessName'	string
     *                       'businessName[]'	array
     *                       'addressLine1'	string
     *                       'addressLine1[]'	array
     *                       'addressLine2'	string
     *                       'addressLine2[]'	array
     *                       'addressLine3'	string
     *                       'addressLine3[]'	array
     *                       'city'	string
     *                       'city[]'	array
     *                       'county'	string
     *                       'county[]'	array
     *                       'countryIso'	string
     *                       'countryIso[]'	array
     *                       'postcode'	string
     *                       'postcode[]'	array
     *                       'telephone'	string
     *                       'telephone[]'	array
     *                       'email'	string
     *                       'email[]'	array
     *                       'usage'	string
     *                       'usage[]'	array
     *                       'order[id]'	string
     *                       'order[clientId]'	string
     *                       'order[usage]'	string
     *
     * @return Read[]|null
     */
    public function defaultCollection(array $queries = []): ?array
    {
        return $this->request(
        'defaultAddressCollection',
        'GET',
        'api/misc/addresses/default/{clientId}/{usage}',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves the collection of Address resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'clientCode'	string
     *                       'isDefault'	boolean
     *                       'isDefault[]'	array
     *                       'name'	string
     *                       'contact'	string
     *                       'contact[]'	array
     *                       'businessName'	string
     *                       'businessName[]'	array
     *                       'addressLine1'	string
     *                       'addressLine1[]'	array
     *                       'addressLine2'	string
     *                       'addressLine2[]'	array
     *                       'addressLine3'	string
     *                       'addressLine3[]'	array
     *                       'city'	string
     *                       'city[]'	array
     *                       'county'	string
     *                       'county[]'	array
     *                       'countryIso'	string
     *                       'countryIso[]'	array
     *                       'postcode'	string
     *                       'postcode[]'	array
     *                       'telephone'	string
     *                       'telephone[]'	array
     *                       'email'	string
     *                       'email[]'	array
     *                       'usage'	string
     *                       'usage[]'	array
     *                       'order[id]'	string
     *                       'order[clientId]'	string
     *                       'order[usage]'	string
     *
     * @return Read[]|null
     */
    public function system_defaultCollection(array $queries = []): ?array
    {
        return $this->request(
        'system_defaultAddressCollection',
        'GET',
        'api/misc/addresses/system_default/{usage}',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a Address resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getAddressItem',
        'GET',
        "api/misc/addresses/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Address resource.
     *
     * @param string $id    Resource identifier
     * @param Update $Model The updated Address resource
     *
     * @return Read
     */
    public function putItem(string $id, Update $Model): Read
    {
        return $this->request(
        'putAddressItem',
        'PUT',
        "api/misc/addresses/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Address resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteAddressItem',
        'DELETE',
        "api/misc/addresses/$id",
        null,
        [],
        []
        );
    }
}
