<?php

namespace LogisticsX\Misc\Api;

use LogisticsX\Misc\Model\SystemConfig\SystemConfig\Read;
use LogisticsX\Misc\Model\SystemConfig\SystemConfig\Write;

class SystemConfig extends AbstractAPI
{
    /**
     * Retrieves the collection of SystemConfig resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'code[]'	array
     *                       'value'	string
     *                       'value[]'	array
     *                       'order[code]'	string
     *                       'order[value]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getSystemConfigCollection',
        'GET',
        'api/misc/system_configs',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a SystemConfig resource.
     *
     * @param Write $Model The new SystemConfig resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postSystemConfigCollection',
        'POST',
        'api/misc/system_configs',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a SystemConfig resource.
     *
     * @param string $code Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $code): ?Read
    {
        return $this->request(
        'getSystemConfigItem',
        'GET',
        "api/misc/system_configs/$code",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the SystemConfig resource.
     *
     * @param string $code  Resource identifier
     * @param Write  $Model The updated SystemConfig resource
     *
     * @return Read
     */
    public function putItem(string $code, Write $Model): Read
    {
        return $this->request(
        'putSystemConfigItem',
        'PUT',
        "api/misc/system_configs/$code",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the SystemConfig resource.
     *
     * @param string $code Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $code): mixed
    {
        return $this->request(
        'deleteSystemConfigItem',
        'DELETE',
        "api/misc/system_configs/$code",
        null,
        [],
        []
        );
    }
}
